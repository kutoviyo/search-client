package skywell.com.skywelltesttask


import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.test.suitebuilder.annotation.LargeTest
import android.view.View
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {
        val appCompatEditText = onView(
                allOf<View>(withId(R.id.repos_search_view), isDisplayed()))
        appCompatEditText.perform(click())

        val appCompatEditText2 = onView(
                allOf<View>(withId(R.id.repos_search_view), isDisplayed()))
        appCompatEditText2.perform(replaceText("tetris"), closeSoftKeyboard())

        val appCompatButton = onView(
                allOf<View>(withId(R.id.submit_search_btn), withText("Search"), isDisplayed()))
        appCompatButton.perform(click())

        val appCompatButton2 = onView(
                allOf<View>(withId(R.id.submit_search_btn), withText("Search"), isDisplayed()))
        appCompatButton2.perform(click())

        val appCompatEditText3 = onView(
                allOf<View>(withId(R.id.repos_search_view), withText("tetris"), isDisplayed()))
        appCompatEditText3.perform(replaceText("te"), closeSoftKeyboard())

        val appCompatButton3 = onView(
                allOf<View>(withId(R.id.submit_search_btn), withText("Search"), isDisplayed()))
        appCompatButton3.perform(click())

        val appCompatButton4 = onView(
                allOf<View>(withId(R.id.submit_search_btn), withText("Stop"), isDisplayed()))
        appCompatButton4.perform(click())

        pressBack()

    }

}

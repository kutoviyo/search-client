package skywell.com.skywelltesttask

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import skywell.com.skywelltesttask.adapter.ReposAdapter
import skywell.com.skywelltesttask.di.AppComponent
import skywell.com.skywelltesttask.di.scopes.ApplicationScope
import skywell.com.skywelltesttask.model.RepositoryItem
import skywell.com.skywelltesttask.presenter.ReposPresenter
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var reposPresenter: ReposPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerMainActivity_Component.builder()
                .appComponent(SkywellApplication.appComponent)
                .build().inject(this)

        setContentView(R.layout.activity_main)
        reposPresenter.attachView(this)

        repositories_list.setHasFixedSize(true)
        repositories_list.layoutManager = LinearLayoutManager(this)
        initAdapter()

        submit_search_btn.setOnClickListener {
            reposPresenter.initializeSearching(repos_search_view.text.toString())
        }
    }

    override fun onDestroy() {
        reposPresenter.detachView()
        super.onDestroy()
    }

    private fun initAdapter() {
        if (repositories_list.adapter == null) {
            repositories_list.adapter = ReposAdapter()
        }
    }

    fun showRepositories(repositories: List<RepositoryItem>) {
        (repositories_list.adapter as ReposAdapter).setRepos(repositories)
        submit_search_btn.text = resources.getString(R.string.search_btn_text)
        submit_search_btn.setOnClickListener {
            reposPresenter.initializeSearching(repos_search_view.text.toString())
        }
    }

    fun showError(errMsg: String) {
        Snackbar.make(repositories_list, errMsg, Snackbar.LENGTH_LONG).show()
    }

    fun setBtnDeclinable() {
        submit_search_btn.setOnClickListener {
            reposPresenter.stopSearching()
        }
        submit_search_btn.text = resources.getString(R.string.decline_search)
    }

    fun setBtnForSearch() {
        submit_search_btn.text = resources.getString(R.string.search_btn_text)
        submit_search_btn.setOnClickListener {
            reposPresenter.initializeSearching(repos_search_view.text.toString())
        }
    }

    @dagger.Component(dependencies = arrayOf(AppComponent::class))
    @ApplicationScope
    internal interface Component {
        fun inject(activity: MainActivity)
    }
}
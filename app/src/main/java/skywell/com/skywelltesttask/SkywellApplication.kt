package skywell.com.skywelltesttask

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.util.Log
import com.facebook.stetho.Stetho
import skywell.com.skywelltesttask.db.Database
import skywell.com.skywelltesttask.di.AppComponent
import skywell.com.skywelltesttask.di.DaggerAppComponent
import skywell.com.skywelltesttask.di.modules.ApplicationModule
import java.lang.ref.WeakReference

class SkywellApplication : Application() {

    private val TAG = SkywellApplication::class.java.simpleName

    private var context: WeakReference<Context>? = null
    private var database: Database? = null

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        Log.d(TAG, "Starting")
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        database = Room.databaseBuilder(this, Database::class.java, "skywell_db").build()
        appComponent = DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(this)).build()
        context = WeakReference(this)
    }

    fun getContext(): Context? {
        return context!!.get()
    }

    fun getDatabase(): Database? {
        return database!!
    }

}
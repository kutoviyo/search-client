package skywell.com.skywelltesttask.adapter

interface ViewType {
    fun getViewType(): Int
}
package skywell.com.skywelltesttask.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import skywell.com.skywelltesttask.R
import skywell.com.skywelltesttask.adapter.ViewType
import skywell.com.skywelltesttask.adapter.ViewTypeDelegateAdapter
import skywell.com.skywelltesttask.common.inflate

class LoadingDelegateAdapter : ViewTypeDelegateAdapter {

    override fun onCreateViewHolder(parent: ViewGroup) = TurnsViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
    }

    class TurnsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.repo_item_loading)) {
    }
}
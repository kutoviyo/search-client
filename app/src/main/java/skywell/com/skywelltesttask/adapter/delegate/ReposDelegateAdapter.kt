package skywell.com.skywelltesttask.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import kotlinx.android.synthetic.main.repo_item.view.*
import skywell.com.skywelltesttask.R
import skywell.com.skywelltesttask.adapter.ViewType
import skywell.com.skywelltesttask.adapter.ViewTypeDelegateAdapter
import skywell.com.skywelltesttask.common.inflate
import skywell.com.skywelltesttask.common.loadImg
import skywell.com.skywelltesttask.model.RepositoryItem

class ReposDelegateAdapter() : ViewTypeDelegateAdapter {

    interface onViewSelectedListener {
        fun onItemSelected(url: String?)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ReposViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, item: ViewType) {
        holder as ReposViewHolder
        holder.bind(item as RepositoryItem)
    }

    inner class ReposViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            parent.inflate(R.layout.repo_item)) {

        fun bind(item: RepositoryItem) = with(itemView) {
            //Picasso.with(itemView.context).load(item.thumbnail).into(img_thumbnail)
            img_thumbnail.loadImg(item.imgUrl)
            description.text = item.description
            author.text = item.name
            name.text = item.fullName
//            super.itemView.setOnClickListener { viewActions.onItemSelected(item.url)}
        }
    }
}
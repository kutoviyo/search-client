package skywell.com.skywelltesttask.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import skywell.com.skywelltesttask.dto.GithubReposResponse

interface GithubApi {

    @GET("/search/repositories")
    fun searchRepositories(@Query("q") q: String, @Query("sort") sort: String): Call<GithubReposResponse>
}
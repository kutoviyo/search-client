package skywell.com.skywelltesttask.common

import android.content.Context

class SharedPreferencesUtil(val context: Context) {

    var PREFERENCES_NAME = "query_cache"
    val QUERY_KEY = "query"

    var prefs = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)

    fun setCachedQuery(query: String ) {
        val prefsEditor = prefs.edit()
        prefsEditor.putString(QUERY_KEY, query)
        prefsEditor.apply()
    }

    fun getCachedQuery(): String {
        return prefs.getString(QUERY_KEY, "")
    }
}
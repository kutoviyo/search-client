package skywell.com.skywelltesttask.controller

import android.util.Log
import io.reactivex.Observable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import skywell.com.skywelltesttask.api.GithubApi
import skywell.com.skywelltesttask.common.SharedPreferencesUtil
import skywell.com.skywelltesttask.db.dao.RepositoryDao
import skywell.com.skywelltesttask.db.entities.Repository
import skywell.com.skywelltesttask.dto.GithubRepository
import skywell.com.skywelltesttask.model.RepositoryItem

class ReposController(private val api: GithubApi,
                      private val reposDao: RepositoryDao,
                      private val prefs: SharedPreferencesUtil) {

    val TAG = "ReposController"

    fun searchRepositories(query: String): Observable<List<RepositoryItem>> {
        Log.e(TAG, query)
        if (cached(query)) {
            Log.d(TAG, "get from db")
            return getFromDb()
        } else {
            cacheQueryString(query)
            return getFromServer(formatQueryString(query))
        }
    }

    fun clearSearch() {
        cacheQueryString("")
    }

    private fun getFromServer(query: String): Observable<List<RepositoryItem>> {
        Log.d(TAG, "getFromServer")
        callRequest(query)
                .subscribeOn(Schedulers.io())
                .onErrorReturn { ArrayList<GithubRepository>() }
                .subscribe({ repository ->
                    reposDao.clear()
                    val reposList = repository.map { item ->
                        Repository(item.id, item.name, item.full_name,
                                item.html_url, item.url, item.size, item.language)
                    }
                    reposDao.insert(reposList)
                })

        return callRequest(query).map { list ->
            list.map { item ->
                RepositoryItem(item.id, item.name, item.full_name, item.url, item.html_url, item.language)
            }
        }
    }

    private fun getFromDb(): Observable<List<RepositoryItem>> {
        return Observable.create { subscriber ->
            val listFromDb = reposDao.getAllRepositories()
                    .blockingFirst()
                    .map { repo ->
                        RepositoryItem(repo.uid, repo.name, repo.fullName, repo.url, repo.htmlUrl, repo.language)
                    }
            if (listFromDb.isNotEmpty()) {
                subscriber.onNext(listFromDb)
                subscriber.onComplete()
            } else {
                subscriber.onError(Throwable("No items cached, something went wrong! "))
            }
        }
    }

    private fun callRequest(query: String): Observable<List<GithubRepository>> {
        return Observable.create { subscriber ->
            val callResponse = api.searchRepositories(query, "stars")
            try {
                val response = callResponse.execute()
                if (response.isSuccessful) {
                    var list = response.body()?.items
                    Log.e(TAG, list.toString())
                    if (list != null) {
                        subscriber.onNext(list)
                        subscriber.onComplete()
                    } else {
                        Log.e(TAG, response.message())
                        subscriber.onError(Throwable(response.message()))
                    }
                } else {
                    Log.e(TAG, response.message())
                    subscriber.onError(Throwable(response.message()))
                }
            } catch (e: Exception) {
                RxJavaPlugins.setErrorHandler { e -> println(e.message) }
            }
        }
    }

    private fun cached(query: String): Boolean {
        var cachedQuery = prefs.getCachedQuery()
        if (cachedQuery.isEmpty())
            return false
        return cachedQuery.equals(query)
    }

    private fun formatQueryString(query: String): String {
        return query.replace(" ", "+", true)
    }

    private fun cacheQueryString(query: String) {
        Log.d(TAG, "cacheQueryString")
        prefs.setCachedQuery(query)
    }
}
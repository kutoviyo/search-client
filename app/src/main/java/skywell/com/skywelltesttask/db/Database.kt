package skywell.com.skywelltesttask.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import skywell.com.skywelltesttask.db.dao.RepositoryDao
import skywell.com.skywelltesttask.db.entities.Repository

@Database(entities = arrayOf(Repository::class), version = 1)
abstract class Database : RoomDatabase() {

    abstract fun repositoryDao(): RepositoryDao
}
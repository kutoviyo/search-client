package skywell.com.skywelltesttask.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import skywell.com.skywelltesttask.db.entities.Repository

@Dao
interface RepositoryDao {

    @Query("SELECT * FROM repository")
    fun getAllRepositories(): Flowable<List<Repository>>

    @Insert
    fun insert(repository: List<Repository>)

    @Query("DELETE FROM repository")
    fun clear()
}
package skywell.com.skywelltesttask.db.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "repository")
data class Repository(
        @PrimaryKey(autoGenerate = true)
        val uid: Long?,
        val name: String? = "",
        val fullName: String? = "",
        val htmlUrl: String? = "",
        val url: String? = "",
        val size: Long? = 0,
        val language: String? = ""
)
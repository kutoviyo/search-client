package skywell.com.skywelltesttask.di

import android.content.Context
import android.content.res.Resources
import dagger.Component
import skywell.com.skywelltesttask.api.GithubApi
import skywell.com.skywelltesttask.common.SharedPreferencesUtil
import skywell.com.skywelltesttask.controller.ReposController
import skywell.com.skywelltesttask.db.Database
import skywell.com.skywelltesttask.db.dao.RepositoryDao
import skywell.com.skywelltesttask.di.modules.*
import skywell.com.skywelltesttask.presenter.ReposPresenter
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        ApplicationModule::class, DaoModule::class, RestModule::class,
        ControllerModule::class, PresenterModule::class, UtilsModule::class)
)
interface AppComponent {

    //APP
    fun getContext(): Context

    fun getResources(): Resources

    fun getDatabase(): Database

    fun getSharedPreferencesUtil(): SharedPreferencesUtil

    //REST
    fun getApi(): GithubApi

    //DAO
    fun getRepositoriesDao(): RepositoryDao

    //PRESENTER
    fun getReposPresenter(): ReposPresenter

    //CONTROLLER
    fun getReposController(): ReposController

    //UTIL
}
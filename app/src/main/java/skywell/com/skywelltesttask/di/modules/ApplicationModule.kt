package skywell.com.skywelltesttask.di.modules

import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import skywell.com.skywelltesttask.SkywellApplication
import skywell.com.skywelltesttask.common.SharedPreferencesUtil
import skywell.com.skywelltesttask.db.Database
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: SkywellApplication) {

    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    internal fun provideResources(): Resources {
        return application.resources
    }

    @Singleton
    @Provides
    internal fun provideDatabase(): Database {
        return application.getDatabase()!!
    }

    @Singleton
    @Provides
    internal fun providePrefsUtil(): SharedPreferencesUtil {
        return SharedPreferencesUtil(application.applicationContext)
    }
}
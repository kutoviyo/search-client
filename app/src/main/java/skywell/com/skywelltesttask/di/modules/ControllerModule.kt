package skywell.com.skywelltesttask.di.modules

import dagger.Module
import dagger.Provides
import skywell.com.skywelltesttask.api.GithubApi
import skywell.com.skywelltesttask.common.SharedPreferencesUtil
import skywell.com.skywelltesttask.controller.ReposController
import skywell.com.skywelltesttask.db.dao.RepositoryDao
import javax.inject.Singleton

@Module
class ControllerModule {

    @Singleton
    @Provides
    internal fun provideReposController(api: GithubApi,
                                        reposDao: RepositoryDao,
                                        preferencesUtil: SharedPreferencesUtil): ReposController {
        return ReposController(api, reposDao, preferencesUtil)
    }
}
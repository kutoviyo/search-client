package skywell.com.skywelltesttask.di.modules

import dagger.Module
import dagger.Provides
import skywell.com.skywelltesttask.db.Database
import skywell.com.skywelltesttask.db.dao.RepositoryDao
import javax.inject.Singleton

@Module
class DaoModule {

    @Singleton
    @Provides
    internal fun provideRepositoryDao(database: Database): RepositoryDao {
        return database.repositoryDao()
    }
}
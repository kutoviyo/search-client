package skywell.com.skywelltesttask.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import skywell.com.skywelltesttask.controller.ReposController
import skywell.com.skywelltesttask.presenter.ReposPresenter
import javax.inject.Singleton

@Module
class PresenterModule {

    @Singleton
    @Provides
    internal fun provideReposPresenter(context: Context, reposController: ReposController): ReposPresenter {
        return ReposPresenter(context, reposController)
    }
}
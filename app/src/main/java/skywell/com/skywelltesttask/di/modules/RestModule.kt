package skywell.com.skywelltesttask.di.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import skywell.com.skywelltesttask.api.GithubApi
import javax.inject.Singleton


@Module
class RestModule {

    @Singleton
    @Provides
    internal fun provideApi(): GithubApi {
        val retrofit = Retrofit.Builder()
                .baseUrl(GITHUB_BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        return retrofit.create(GithubApi::class.java)
    }

    companion object {
        val GITHUB_BASE_URL = "https://api.github.com"
    }
}
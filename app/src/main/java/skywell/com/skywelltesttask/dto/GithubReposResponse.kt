package skywell.com.skywelltesttask.dto

class GithubReposResponse(
        val items: List<GithubRepository>,
        val total_count: Long,
        val before: String?
){
    override fun toString(): String {
        return "GithubReposResponse(items=$items, total_count=$total_count, before=$before)"
    }
}

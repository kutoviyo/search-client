package skywell.com.skywelltesttask.dto

class GithubRepository(
        val id: Long,
        val name: String,
        val full_name: String,
        val html_url: String,
        val url: String,
        val size: Long,
        val language: String
)
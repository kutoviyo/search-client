package skywell.com.skywelltesttask.model

import skywell.com.skywelltesttask.adapter.AdapterConstants
import skywell.com.skywelltesttask.adapter.ViewType

data class RepositoryItem(
        var id: Long?,
        var name: String?,
        var fullName: String?,
        var url: String?,
        var imgUrl: String?,
        var description: String?
): ViewType {
    override fun getViewType() = AdapterConstants.REPOS
}
package skywell.com.skywelltesttask.presenter

import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import skywell.com.skywelltesttask.MainActivity
import skywell.com.skywelltesttask.controller.ReposController
import skywell.com.skywelltesttask.model.RepositoryItem

class ReposPresenter(private val context: Context, private val reposController: ReposController) {

    private var view: MainActivity? = null
    private var subscriber: Disposable? = null

    fun attachView(view: MainActivity) {
        this.view = view
    }

    fun detachView() {
        view = null
    }

    fun initializeSearching(queryString: String) {
        if(queryString.isEmpty())
            return

        view!!.setBtnDeclinable()
        subscriber = reposController.searchRepositories(queryString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { ArrayList<RepositoryItem>() }
                .subscribe(
                        { repository ->
                            Log.e("PRESENTER", "show repositories")
                            view!!.showRepositories(
                                    repository.map {
                                        RepositoryItem(it.id, it.name, it.fullName,
                                                it.url, it.imgUrl, it.description)
                                    })
                        },
                        { e ->
                            view!!.showError(e.message!!)
                        }
                )
    }

    fun stopSearching() {
        Log.e("PRESENTER", "stop searching")
        subscriber?.dispose()
        reposController.clearSearch()
        view!!.setBtnForSearch()
    }
}

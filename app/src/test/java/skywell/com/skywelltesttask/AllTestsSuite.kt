package skywell.com.skywelltesttask

import org.junit.runner.RunWith
import org.junit.runners.Suite
import skywell.com.skywelltesttask.unittestssuite.UnitTestsSuite

@RunWith(Suite::class)
@Suite.SuiteClasses(UnitTestsSuite::class)
class AllTestsSuite
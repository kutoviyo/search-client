package skywell.com.skywelltesttask.unittestssuite

import org.junit.runner.RunWith
import org.junit.runners.Suite
import skywell.com.skywelltesttask.unittestssuite.controllers.ReposControllerTestsSuite

@RunWith(Suite::class)
@Suite.SuiteClasses(ReposControllerTestsSuite::class)
class UnitTestsSuite